//
//  Model.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import SwiftUI

struct ToDo: Identifiable, Equatable {
    var id = UUID().uuidString
    let name, target: String
    let date: Date
    let priority: Priority
}

enum Priority: Int, Identifiable, CaseIterable {
    var id: Int { rawValue }
    case normal, medium, high
    var title: String {
        switch self {
        case .normal:
            return "Normal"
        case .medium:
            return "Medium"
        case .high:
            return "High"
        }
    }
    var color: Color {
        switch self {
        case .normal:
            return .blue
        case .medium:
            return .orange
        case .high:
            return .red
        }
    }
}

enum SortType: String, Identifiable, CaseIterable {
    var id: String { rawValue }
    case order, date, priority
}

