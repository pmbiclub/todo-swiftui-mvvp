//
//  ContentViewMod.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import SwiftUI

struct ContentViewMod: ViewModifier {
    @ObservedObject var vm: ViewModel
    func body(content: Content) -> some View {
        content
            .navigationTitle("My ToDo's")
            .navigationBarItems(
                trailing:
                    HStack(spacing: 30) {
                        EditButton().disabled(vm.todos.isEmpty)
                        Button(action: {vm.isPresented.toggle()},
                               label: {
                                Image(systemName: "plus")
                                    .imageScale(.large)
                               })
                    })
            .onChange(of: vm.sortType) { _ in
                guard !vm.todos.isEmpty else { return }
                withAnimation() { vm.sort() }
            }
            .fullScreenCover(isPresented: $vm.isPresented) {
                AddToDoView(vm: vm)
                
            }
    }
}
