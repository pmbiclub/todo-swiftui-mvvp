//
//  TODO_MVVP_SWIFTUIApp.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import SwiftUI

@main
struct TODO_MVVP_SWIFTUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
