//
//  ViewModel.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import Foundation

final class ViewModel: ObservableObject {
    @Published var todos: [ToDo] = []
    
    @Published var sortType: SortType = .order
    @Published var isPresented = false
    
    func addToDo (_ todo: ToDo) {
        DispatchQueue.main.async { [self] in
            todos.append(todo)
            
        }
    }
    
    func removeTodo (_ indexSet: IndexSet) {
        DispatchQueue.main.async{ [self] in
            todos.remove(atOffsets: indexSet)
        }
    }
    
    func sort() {
        switch sortType {
        case .order:
            todos.sort(by: { $0.name < $1.name })
        case .date:
            todos.sort(by: { $0.date < $1.date})
        case .priority:
            todos.sort(by: { $0.priority.rawValue < $1.priority.rawValue})
        }
    }
}
