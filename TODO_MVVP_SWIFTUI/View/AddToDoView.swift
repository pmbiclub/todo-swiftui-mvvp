//
//  AddToDoView.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import SwiftUI

struct AddToDoView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var vm: ViewModel
    @State private var name = ""
    @State private var target = ""
    @State private var date = Date()
    @State private var priority: Priority = .normal
    
    
    var body: some View {
        NavigationView{
            Form{
                Section(header: Text("todo")) {
                    TextField("name....", text: $name)
                    TextField("target....", text: $target)
                }
                Section{
                    Picker("Priority", selection: $priority) {
                        ForEach(Priority.allCases) {
                            priority in
                            Label(
                                title: { Text(priority.title) },
                                icon: { Image(systemName: "exclamationmark.circle") })
                                .foregroundColor(priority.color)
                                .tag(priority)
                        }
                    }
                }
                DisclosureGroup("Date") {
                    DatePicker("", selection: $date)
                        .datePickerStyle(GraphicalDatePickerStyle())
                }
            }
            .navigationBarTitle("Add", displayMode: .inline)
            .navigationBarItems(
                leading:
                    Button(action: {presentationMode.wrappedValue.dismiss()},
                           label: {
                            Text("Cancel")
                                .foregroundColor(.red)
                           })
                ,
                trailing:
                    Button(action: {
                            vm.addToDo(.init(
                                        name: name,
                                        target: target,
                                        date: date,
                                        priority: priority))
                            presentationMode.wrappedValue.dismiss()},
                           label: {
                            Text("Save")
                           })
                    .disabled(name.isEmpty || target.isEmpty)
            )
        }
    }
}

struct AddToDoView_Previews: PreviewProvider {
    static var previews: some View {
        AddToDoView(vm: ViewModel())
    }
}
