//
//  SortPickerView.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import SwiftUI

struct SortPickerView: View {
    @ObservedObject var vm: ViewModel
    var body: some View {
        Picker("", selection: $vm.sortType) {
            ForEach(SortType.allCases){
                Text($0.rawValue.capitalized)
                    .tag($0)
            }
        }
        .pickerStyle(SegmentedPickerStyle())
        .padding()
    }
}

struct SortPickerView_Previews: PreviewProvider {
    static var previews: some View {
        SortPickerView(vm: ViewModel())
    }
}
