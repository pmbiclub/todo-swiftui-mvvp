//
//  ContentView.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import SwiftUI

struct ContentView: View {
    @StateObject private var vm = ViewModel()
    var body: some View {
        NavigationView {
            VStack{
                SortPickerView(vm: vm)
                ToDoListView(vm: vm)
            }
            .modifier(ContentViewMod(vm: vm))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
