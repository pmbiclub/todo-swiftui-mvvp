//
//  ToDoView.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import SwiftUI

struct ToDoView: View {
    var todo: ToDo
    
    var body: some View {
        
        VStack (alignment: .leading){
            HStack {
                VStack(alignment: .leading) {
                    Text("Name:")
                        .font(.caption)
                        .foregroundColor(.secondary)
                    Text(todo.name)
                }
                Spacer()
                VStack(alignment: .leading){
                    Text("Date:")
                        .font(.caption)
                        .foregroundColor(.secondary)
                    Text(todo.date, style: .date)
                }
            }
            Text("Target")
                .font(.caption)
                .foregroundColor(.secondary)
            Text(todo.target)
                .italic()
            
        }
        .padding(10)
        .background(todo.priority.color.opacity(0.15))
        .cornerRadius(10)
        .background(RoundedRectangle(cornerRadius: 10, style: .continuous)
                        .stroke(todo.priority.color, lineWidth: 0.7)
                        .shadow(color: todo.priority.color, radius: 0.7 ))
    }
}

struct ToDoView_Previews: PreviewProvider {
    static var previews: some View {
        ToDoView(todo: ToDo(id: "", name: "Some Name", target: "Some Target", date: Date(), priority: .normal))
    }
}
