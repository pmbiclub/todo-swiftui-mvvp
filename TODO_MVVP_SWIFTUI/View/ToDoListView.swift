//
//  ToDoListView.swift
//  TODO_MVVP_SWIFTUI
//
//  Created by Artyom Romanchuk on 01.01.2021.
//

import SwiftUI

struct ToDoListView: View {
    @ObservedObject var vm: ViewModel
    var body: some View {
        List {
            ForEach(vm.todos){ todo in
                ToDoView(todo: todo)
                
            }.onDelete {vm.removeTodo($0)}
        }
        .listStyle(InsetListStyle())
    }
}

struct ToDoListView_Previews: PreviewProvider {
    static var previews: some View {
        ToDoListView(vm: ViewModel())
    }
}
